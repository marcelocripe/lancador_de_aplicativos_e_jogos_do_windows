Lançador ou Inicializador de Aplicativos e Jogos do Windows

windows-apps-games-launcher.sh
windows-apps-games-launcher.desktop
Lancador de Aplicativos e Jogos do Windows pt-BR.txt 
locale: windows-apps-games-launcher.pot, pt_BR: windows-apps-games-launcher.po e windows-apps-games-launcher.mo



Índice:

- - - - - Introdução - - - - -
- - - - - Processo de instalação (cópia dos arquivos) - - - - -
- - - Metódo 1 - - -
- - - Metódo 2 - - -
- - - - - Traduções - - - - -



- - - - - Introdução - - - - -

O "Lançador ou Inicializador de Aplicativos e Jogos do Windows" é uma interface gráfica que permite listar e lançar/inicializar no programa Wine todos os aplicativos e os jogos compatíveis com o sistema operacional Windows da Microsoft que estiverem em uma pasta.

Quando o programa for executado pela primeira vez, será exibida uma janela que permite escolher em qual pasta estão os seus aplicativos e os seus jogos. Os aplicativos e os jogos podem estar nas suas respectivas pastas (ou subpastas). Por exemplo, mova as pastas dos aplicativos e dos jogos para dentro da pasta "Aplicativos_e_Jogos_do_Windows" e escolha esta pasta na janela que permite escolher em qual pasta estão os seus aplicativos e os seus jogos do Windows.

O programa "windows-apps-games-launcher.sh" está escrito em idioma Português de Portugal (Europeu), as traduções estão disponíveis para cerca de 108 idiomas, sendo que apenas os idiomas "de", "en", "en_GB", "en_US", "fr", "fr_BE", "it", "pt", "pt_BR" foram feitas pelos tradutores voluntários da comunidade antiX, os outros idiomas são traduções automáticas que necessitam da revisão dos nativos dos respectivos idiomas.

A camada de compatibilidade Wine precisa estar instalada no seu sistema operacional para o "Lançador de Aplicativos e Jogos do Windows" funcionar corretamente.



- - - - - Processo de instalação (cópia dos arquivos) - - - - -

A instalação do programa é na verdade uma cópia dos arquivos para as pastas corretas no seu sistema operacional e existem dois métodos disponíveis neste documento.

Como utilizar o conjunto de instruções (script) "windows-apps-games-launcher.sh", o ícone de atalho "windows-apps-games-launcher.desktop" e as traduções:

Em um computador/laptop que possua dois ou mais usuários, onde todos os usuários podem utilizar o programa, utilize o Metódo 1.
Em um computador/laptop que possua dois ou mais usuários, mas que você não queira que os outros usuários utilizem o programa, utilize o Metódo 2.


- - - Metódo 1 - - -

Acesse o gerenciador de arquivos SpaceFM (no antiX 19) ou zzzFM (no antiX 21, 22 e 23), clique no Menu do antiX, Aplicativos/Aplicações, Sistema, SpaceFM ou zzzFM.

Considerando que você baixou/transferiu/descarregou o arquivo compactado "Lancador de Aplicativos e Jogos do Windows.zip" para a pasta "Baixados" ou "Transferências" ou "Downloads", clique com o botão direito do rato/mouse sobre o arquivo "Lancador de Aplicativos e Jogos do Windows.zip", Abrir/Open, Extrair Aqui/Extract.

A pasta "windows-apps-games-launcher" será extraída, clique no menu Arquivo/File do SpaceFM ou zzzFM, Abrir Nova Janela como Administrador/Open Root Window, digite a sua senha, Ok, abrirá uma nova janela em cor vermelha, clique com o botão direito do rato/mouse sobre a pasta "windows-apps-games-launcher", Recortar/Cut.

Com a janela da cor vermelha, acesse a pasta ou diretório /opt, clique com o botão direito do rato/mouse na área branca e em seguida clique na opção Colar/Paste.

Entre na pasta "windows-apps-games-launcher", você encontrará o programa "windows-apps-games-launcher.sh", o ícone de atalho "windows-apps-games-launcher.desktop" e a pasta "locale".

Clique com o botão direito do rato/mouse sobre o ícone de atalho "windows-apps-games-launcher.desktop", Copiar/Copy, acesse /usr/share/applications, clique com o botão direito do rato/mouse na área branca e em seguida clique em Colar/Paste.

Para exibir no menu do antiX o novo ícone de atalho "windows-apps-games-launcher.desktop", clique no Menu do antiX, Terminal, digite o comando "$ sudo desktop-menu --write-out-global" (sem as aspas duplas e sem o sinal de cifrão "$"), pressione a tecla "Enter", insira a sua senha e pressione a tecla "Enter", o novo menu "Lançador de Aplicativos e Jogos do Windows" será adicionado no Menu do antiX, Aplicativos/Aplicações, Jogos.

O ícone de atalho "windows-apps-games-launcher.desktop" está configurado para encontrar o script "windows-apps-games-launcher.sh" no caminho /opt/windows-apps-games-launcher e a imagem "microsoft.png" no caminho /usr/share/icons/papirus-antix/48x48/apps/.


- - - Metódo 2 - - -

Acesse o gerenciador de arquivos SpaceFM (no antiX 19) ou zzzFM (no antiX 21, 22 e 23), clique no Menu do antiX, Aplicativos/Aplicações, Sistema, SpaceFM ou zzzFM.

Considerando que você baixou/transferiu/descarregou o arquivo compactado "Lancador de Aplicativos e Jogos do Windows.zip" na pasta "Baixados" ou "Transferências" ou "Downloads", clique com o botão direito do rato/mouse sobre o arquivo "Lancador de Aplicativos e Jogos do Windows.zip", Abrir/Open, Extrair Aqui/Extract.

A pasta "windows-apps-games-launcher" será extraída, clique com o botão direito do rato/mouse sobre a pasta "windows-apps-games-launcher", Recortar/Cut, clique na seta para cima para subir o nível e acessar a sua pasta pessoal que deverá estar no caminho /home/nome de usuario, clique com o botão direito do rato/mouse na área branca embaixo da pasta "Vídeos" e em seguida clique na opção Colar/Paste.

Clique com o botão direito do rato/mouse sobre a pasta "windows-apps-games-launcher", Renomear/Rename, digite um ponto "." sem as aspas no início do nome "windows-apps-games-launcher" que ficará ".windows-apps-games-launcher", clique no botão "Renomear". A pasta ".windows-apps-games-launcher" imediatamente ficará oculta, portanto não se assuste quando ela sumir. Para visualizar a pasta novamente, clique com o botão direito do rato/mouse na área branca embaixo da pasta "Vídeos" e em seguida clique na opção Exibir/View, Arquivos Ocultos/Hidden Files, você visualizará várias pastas e arquivos de configurações ocultos do antiX. Tenha cuidado para não apagar nenhum destes arquivos ou pastas por engando.

Entre na pasta "windows-apps-games-launcher", você encontrará o programa "windows-apps-games-launcher.sh", o ícone de atalho "windows-apps-games-launcher.desktop" e a pasta "locale".

Clique no menu Arquivo/File do SpaceFM ou zzzFM, Abrir Nova Janela como Administrador/Open Root Window, digite a sua senha, Ok, abrirá uma nova janela em cor vermelha, clique com o botão direito do rato/mouse sobre o ícone de atalho "windows-apps-games-launcher.desktop", Copiar/Copy, acesse /usr/share/applications, clique com o botão direito do rato/mouse na área branca e em seguida clique na opção Colar/Paste.

Para o ícone de atalho funcionar corretamente, clique com o botão direito do rato/mouse sobre o ícone de atalho "windows-apps-games-launcher.desktop", Abrir/Open, clique sobre o editor de texto Leafpad ou Geany.

Localize a linha "Exec=/bin/sh -c '/opt/windows-apps-games-launcher/windows-apps-games-launcher.sh'" (sem as aspas duplas), altere o caminho para "Exec=/bin/sh -c '$HOME/.windows-apps-games-launcher/windows-apps-games-launcher.sh'" (sem as aspas duplas), clique no menu Arquivo, Salvar e feche o arquivo.

Para exibir no menu do antiX o novo ícone de atalho "windows-apps-games-launcher.desktop", clique em Menu do antiX, Terminal, digite o comando "$ sudo desktop-menu --write-out-global" (sem as aspas duplas e sem o sinal de cifrão "$"), pressione a tecla "Enter", insira a sua senha e pressione a tecla "Enter", o novo menu "Lançador de Aplicativos e Jogos do Windows" será adicionado no Menu do antiX, Aplicativos/Aplicações, Jogos.

O ícone de atalho "windows-apps-games-launcher.desktop" foi configurado por você para encontrar o programa "windows-apps-games-launcher.sh" no caminho /home/nome de usuario/.windows-apps-games-launcher, onde está escrito "nome de usuario" deve ser substituído pelo seu nome de usuário e a imagem "microsoft.png" no caminho /usr/share/icons/papirus-antix/48x48/apps/.



- - - - - Traduções - - - - -

Como utilizar a tradução para o idioma Português do Brasil para o "Metódo 1" ou para o "Metódo 2":

Para utilizar a tradução do "Lançador de Programas Aplicativos e Jogos do Windows", na janela em cor vermelha do SpaceFM ou zzzFM, acesse a pasta "windows-apps-games-launcher", entre na pasta locale, você encontrará o arquivo "windows-apps-games-launcher.pot" que é o arquivo modelo para criar outras traduções para vários idiomas, o arquivo "windows-apps-games-launcher_em_branco.po" em branco que pode ser utilizado para criar uma nova tradução e várias pastas contendo as iniciais do nome do idioma e algumas pastas contendo as iniciais do nome do idioma com um traço sublinhado e a sigla do nome do país. Por exemplo, a pasta contendo a sigla "pt_BR" identifica o idioma Português e o país Brasil.

Entre na pasta pt_BR, copie o arquivo de tradução "windows-apps-games-launcher.mo" e cole na pasta /usr/share/locale/pt_BR/LC_MESSAGES.



- - - - - Créditos - - - - -


- - - Créditos do programa "windows-apps-games-launcher.sh" e do ícone de atalho "windows-apps-games-launcher.desktop": - - - 

O programa "ms-dos-apps-games-launcher.sh" foi criado por PPC no dia 12-01-2021 em idioma Português de Portugal/Europeu, o nome original do programa era "dosbox-gui-teste2021.sh" e é um "Lançador de Programas Aplicativos e Jogos do MS-DOS".
O programa "dosbox-gui-teste2021.sh" foi editado e adaptado por marcelocripe no dia 10-07-2022 e passou a ser chamar "windows-apps-games-launcher.sh", agora é um "Lançador de Programas Aplicativos e Jogos do Windows" que utiliza a camada de compatibilidade do Wine para executar os aplicativos e os jogos do Windows (Eu agradeço ao PPC por criar este programa e por me dar este presente gratuitamente. Muito obrigado!).
As traduções automáticas do programa "windows-apps-games-launcher.sh" foram realizadas em 2022 por BobC.
As traduções do programa "ms-dos-apps-games-launcher.sh" foram coletadas na plataforma de tradução https://app.transifex.com/antix-linux-community-contributions/antix-contribs/ms-dos-appsgames-launcher/ nos dias 24 a 25-04-2023 por marcelocripe.
O ícone de atalho "windows-apps-games-launcher.desktop" foi criado por marcelocripe.


Estas são as orientações do PPC sobre a licença:
"Licença GPL - faça o que quiser com o script, mas, por favor, mantenha as linhas sobre o autor, data e licença."



marcelocripe
10-07-2022
