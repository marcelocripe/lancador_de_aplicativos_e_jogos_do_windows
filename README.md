Lançador de Aplicativos/Aplicações e Jogos do Windows; Windows-Anwendung und Game Launcher;  Windows Application and Game Launcher; Application Windows et lanceur de jeu; Avvio applicazioni e giochi Windows

pt-BR:

Contém: o conjunto de instruções (script) com textos em idioma Português de Portugal/Europeu preparado para receber a tradução para vários idiomas, possui a tradução para o idioma Português do Brasil (pt_BR), Inglês (en), Francês (fr), Francês da Bélgica (fr_BE), Italiano (it), o ícone de atalho ou arquivo ".desktop" em idiomas "pt", "pt_BR", "de", "en", "fr", "it"  e um arquivo de texto em idioma "pt-BR" e "it" explicando como utilizá-los.

Baixe/descarregue/transfira o arquivo compactado ".zip" e o arquivo de texto ".txt".

Todos os créditos e direitos estão incluídos nos arquivos em respeito ao trabalho voluntário de cada pessoa que participou e colaborou para que estes arquivos pudessem ser disponibilizados nesta página eletrônica.

marcelocripe

- - - - -

de:

Enthält: den Satz von Anweisungen (Skript) mit Texten in der portugiesischen Sprache von Portugal/Europäisch vorbereitet, um die Übersetzung in mehrere Sprachen zu erhalten, hat die Übersetzung für die portugiesische Sprache von Brasilien (pt_BR), Englisch (en), Französisch (fr) , belgisches Französisch (fr_BE), Italienisch (it), das Verknüpfungssymbol oder die „.desktop“-Datei in den Sprachen „pt“, „pt_BR“, „de“, „en“, „fr“, „it“ und a Textdatei in der Sprache "pt-BR" und "it", die erklärt, wie man sie benutzt.

Laden Sie die komprimierte „.zip“-Datei und die „.txt“-Textdatei herunter.

Alle Credits und Rechte sind in den Dateien in Bezug auf die freiwillige Arbeit jeder Person enthalten, die teilgenommen und mitgearbeitet hat, damit diese Dateien auf dieser Website verfügbar gemacht werden konnten.

marcelocripe

- - - - -

en:

Contains: the set of instructions (script) with texts in the Portuguese language of Portugal/European prepared to receive the translation into several languages, has the translation for the Portuguese language of Brazil (pt_BR), English (en), French (fr), Belgian French (fr_BE), Italian (it), the shortcut icon or ".desktop" file in "pt", "pt_BR", "de", "en", "fr", "it" languages and a file of text in language "pt-BR" and "it" explaining how to use them.

Download the ".zip" compressed file and the ".txt" text file.

All credits and rights are included in the files in respect of the voluntary work of each person who participated and collaborated so that these files could be made available on this website.

marcelocripe

- - - - -

fr:

Contient : le jeu d'instructions (script) avec des textes en langue portugaise du Portugal/européen préparé pour recevoir la traduction en plusieurs langues, a la traduction pour la langue portugaise du Brésil (pt_BR), anglais (en), français (fr) , français belge (fr_BE), italien (it), l'icône de raccourci ou fichier ".desktop" dans les langues "pt", "pt_BR", "de", "en", "fr", "it" et un fichier de texte en langage "pt-BR" et "it" expliquant comment les utiliser.

Téléchargez le fichier compressé ".zip" et le fichier texte ".txt".

Tous les crédits et droits sont inclus dans les fichiers en ce qui concerne le travail bénévole de chaque personne qui a participé et collaboré afin que ces fichiers puissent être mis à disposition sur ce site Web.

marcelocripe

- - - - -

it:

Contiene: il set di istruzioni (script) con testi in lingua portoghese del Portogallo/europeo preparato per ricevere la traduzione in più lingue, ha la traduzione per la lingua portoghese del Brasile (pt_BR), inglese (en), francese (fr) , belga francese (fr_BE), italiano (it), l'icona di scelta rapida o il file ".desktop" nelle lingue "pt", "pt_BR", "de", "en", "fr", "it" e un file di testo in lingua "pt-BR" e "it" che spiega come usarli.

Scarica il file compresso ".zip" e il file di testo ".txt".

Tutti i crediti ei diritti sono inclusi nei file in relazione al lavoro volontario di ogni persona che ha partecipato e collaborato affinché questi file potessero essere resi disponibili su questo sito web.

marcelocripe
